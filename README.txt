================================================================
           _         _               _                          
 _ __ ___ (_)___ ___(_)_ __   __ _  | |__   ___  _ __ ___   ___ 
| '_ ` _ \| / __/ __| | '_ \ / _` | | '_ \ / _ \| '_ ` _ \ / _ \
| | | | | | \__ \__ \ | | | | (_| | | | | | (_) | | | | | |  __/
|_| |_| |_|_|___/___/_|_| |_|\__, | |_| |_|\___/|_| |_| |_|\___|
                             |___/                              
================================================================

Missing home is a survivor game with simple puzzles set in the future
in a post-apocalyptic Earth, where resources are scarce and surviving
is its own challenge.

The player is immersed in a journey as Ross, an engineer trying to
figure out how to escape the desolated land.

In order to control the character movement, the player can use arrow 
keys or left analog (L3). To interact with the environment, press X
both on the keyboard and controller. To drop an item, press Z on the 
keyboard and □  on the controller. Finally, to restore oxygen, press
R in the keyboard and ○  in the controller.
