extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

signal bottle_state
signal cant_collect
signal cant_clean
signal cant_use
signal can_move
signal used_water

export (bool) var empty
export (bool) var clean

var bottle_empty = preload("res://WaterBottle/GAgua_Vazia.svg")
var bottle_full  = preload("res://WaterBottle/GLama_7.svg")
var bottle_clean = preload("res://WaterBottle/GAgua_7.svg")

onready var bottle_sprite = get_node("Sprite")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	empty = true
	clean = false
	bottle_sprite.set_texture(bottle_empty)
	emit_signal("bottle_state", 0)

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func collect_water():
	if empty:
		# success
		clean = false
		empty = false
		$CollectWaterTimer.start()
		emit_signal("can_move",0)
	else:
		emit_signal("cant_collect")

func clean_water():
	if not empty and not clean:
		clean = true
		bottle_sprite.set_texture(bottle_clean)
		emit_signal("bottle_state", 1)
	else:
		emit_signal("cant_clean")

func use_water():
	if not empty and clean:
		empty = true
		clean = false
		emit_signal("can_move",0)
		$UseWaterTimer.start()
	else:
		emit_signal("cant_use")

func _on_CollectWaterTimer_timeout():
	bottle_sprite.set_texture(bottle_full)
	$CollectWaterTimer.stop()
	emit_signal("can_move", 1)
	emit_signal("bottle_state", 2)

func _on_UseWaterTimer_timeout():
	bottle_sprite.set_texture(bottle_empty)
	$UseWaterTimer.stop()
	emit_signal("used_water")
	emit_signal("can_move",1)
	emit_signal("bottle_state", 0)