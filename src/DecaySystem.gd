extends Node

export (float) var decay_per_seconds = 1
export (float) var value = 100

signal maximum_decay

func _ready():
	pass

func _process(delta):
	var d = decay_per_seconds*delta
	if(d < value):
		value -= d
	else:
		value = 0
		emit_signal('maximum_decay')
		queue_free()