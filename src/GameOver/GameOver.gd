extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var exit_flag

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	exit_flag = false
	$GameOverTimer.start()
	$Game_over.play()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _input(event):
	if exit_flag:
		get_tree().change_scene("res://SplashScreen/SplashScreen.tscn")
		queue_free()

func _on_GameOverTimer_timeout():
	$GameOverTimer.stop()
	exit_flag = true
