extends Sprite

signal explosion_finished

func _ready():
	$AnimationPlayer.play("Fade out")

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func on_explosion_finished(anim_name):
	queue_free()
	emit_signal('explosion_finished')
