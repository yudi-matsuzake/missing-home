extends CanvasLayer

export (String, FILE) var dialog_file

var dialogs = Array()
var dialog_index = 0

var can_pass = false

signal dialog_init
signal dialog_finished

func _ready():
	var file = File.new()
	if file.open(dialog_file, file.READ) != 0:
		print('Error opening file: ', dialog_file)
		
	while not file.eof_reached():
		var line = file.get_line()
		if line.strip_edges().length():
			dialogs.append(line)

	$Label.text = dialogs[dialog_index]
	emit_signal('dialog_init')

func _process(delta):
	if Input.is_action_just_pressed('ui_accept'):
		next_dialog()

func next_dialog():
	if not can_pass:
		return

	dialog_index += 1
	if dialog_index < dialogs.size():
		$Label.text = dialogs[dialog_index]
	else:
		emit_signal('dialog_finished')

func on_timeout():
	can_pass = true
