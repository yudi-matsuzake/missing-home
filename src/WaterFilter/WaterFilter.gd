extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

# state 0 -> available
#       1 -> loading
#       2 -> loaded
export (int) var state
signal clean

signal cant_remove
signal cant_filter

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	state = 0
	$AnimatedSprite.animation = "available"

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	# pass

func remove_water_bottle():
	if state == 2:
		# successful removal
		state = 0
		$AnimatedSprite.animation = "available"
	else:
		# failed removal
		emit_signal("cant_remove")

func filter_water_bottle():
	if state == 0:
		# sucessful filtering
		state = 1
		$AnimatedSprite.animation = "loading"
		$FilterTimer.start()
		$Filter.play()
		
	else:
		# failed filtering
		emit_signal("cant_filter")

func _on_FilterTimer_timeout():
	state = 2
	$AnimatedSprite.animation = "loaded"
	$FilterTimer.stop()
	emit_signal("clean")
