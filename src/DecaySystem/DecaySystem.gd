extends Node

export (float) var decay_per_seconds = 1
export (float) var value = 100
export (float) var low_oxygen_threshold = 50

signal maximum_decay
signal low_oxygen

var low_oxygen

func _ready():
	low_oxygen = false

func _process(delta):
	var d = decay_per_seconds*delta
	
	# We use a low_oxygen flag to prevent excessive signal emitions
	if not low_oxygen and value < low_oxygen_threshold:		# oxygen low
		low_oxygen = true
		emit_signal("low_oxygen", 0)
	elif low_oxygen and value >= low_oxygen_threshold:		# oxygen high
		low_oxygen = false
		emit_signal("low_oxygen", 1)
		
	if(d < value):
		value -= d
	else:
		value = 0
		emit_signal('maximum_decay')
		#queue_free()
