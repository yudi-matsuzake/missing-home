extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var init_game = true

func _ready():
	$Area2D/Button.animation = "off"

func _process(delta):
	if Input.is_action_just_pressed('ui_accept'):
		$Area2D/Button.animation = "on"
	elif Input.is_action_just_released('ui_accept'):
		init_game()

func init_game():
	if init_game:
		init_game = false
		$AnimationPlayer.play('TitleAnimation')
		yield($AnimationPlayer, 'animation_finished')
		get_tree().change_scene("res://Main/Main.tscn")
		queue_free()
	$Fundo_inicio.play()