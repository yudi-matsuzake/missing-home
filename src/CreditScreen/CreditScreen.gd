extends Node

func _ready():
	$credit_music.play()

func _process(delta):
	if Input.is_action_just_released('ui_accept'):
		get_tree().change_scene("res://SplashScreen/SplashScreen.tscn")
		queue_free()