extends Node

signal bottle_state

var oxygen_tank

var in_lake
var in_river
var in_filter
var with_bottle
var collided_obj
var died
var recharging
var in_rocket_top
var in_rocket_mid
var in_rocket_bot
var rocket_top_clean
var cleaning_rocket
var collided
var river_side

var in_board
var board_clean
var cleaning_board
var with_board
var with_bot
var with_mid
var with_top

var count_rocket

const OXYGEN_BELOW_THRESH = 0
const OXYGEN_ABOVE_THRESH = 1

func _ready():
	$Explosion.show()
	$Player.position = $StartPosition.position
	died = false
	collided_obj = false
	in_lake = false
	in_filter = false
	in_river = false
	with_bottle = false
	recharging = false
	in_rocket_top = false
	in_rocket_mid = false
	in_rocket_bot = false
	rocket_top_clean = false
	cleaning_rocket = false
	in_board = false
	board_clean = false
	cleaning_board = false
	collided = false
	with_board = false
	river_side = 0
	with_bot = false
	with_mid = false
	with_top = false
	emit_signal("bottle_state", 3)
	$fundo_f1.play()

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	if died:
		return

	if not collided and collided_obj:
		collided = true

	if collided and $Player.is_walking:
		collided = false
		in_lake = false
		in_filter = false
		in_rocket_top = false
		in_river = false

func _on_Player_hit(obj):
	if died:
		return
	collided_obj = obj
	var obj_name = obj.get_name()
	var grandparent = obj.get_parent().get_parent()

	if grandparent and grandparent.get_name() == "Lakes":
		in_lake = true
	elif grandparent and grandparent.get_name() == "Rivers":
		in_river = true
	elif obj_name == "WaterFilter":
		in_filter = true
	elif obj_name == "WaterBottle":
		emit_signal("bottle_state", 4)
		with_bottle = true
		obj.hide()
		$coletar.play()
		obj.get_node("CollisionShape2D").disabled = true
	elif obj_name == "RocketMiddle":
		$RocketMiddle.position = $RocketMiddlePosition.position
		$RocketMiddle.rotation_degrees = 0
		$coletar.play()
		collided_obj = false
		create_dialog("res://assets/dialogs/12-as-the-player-grabs-the-final-rocket-piece.res")
		with_mid = true
		in_rocket_mid = true
	elif obj_name == "RocketBot":
		$RocketBot.position = $RocketBotPosition.position
		$coletar.play()
		collided_obj = false
		create_dialog("res://assets/dialogs/09-as-the-player-grabs-the-second-rocket-part.res")
		with_bot = true
		in_rocket_bot = true
	elif obj_name == "RocketUp":
		if rocket_top_clean:
			$RocketUp.position = $RocketUpPosition.position
			$RocketUp.rotation_degrees = 0
			$coletar.play()
			collided_obj = false
			create_dialog("res://assets/dialogs/03-as-the-player-revels-the-rocket-part.res")
			with_top = true
		in_rocket_top = true
	if obj.get_parent() and obj.get_parent().get_name() == "Board":
		in_board = true
		if board_clean:
			$Rivers/Board.hide()
			$Rivers/Board/StaticBody2D/CollisionShape2D.disabled = true
			create_dialog("res://assets/dialogs/04-as-the-player-grabs-the-board.res")
			with_board = true
			$coletar.play()
			collided_obj = false

func _on_WaterFilter_clean():
	$WaterBottle.clean_water()

func _on_Player_main_button():
	if died:
		return
	if in_lake and with_bottle:
		$WaterBottle.collect_water()
	elif in_river and with_board:
		if river_side == 0:
			river_side = 1
			$Player.position = $Side1Position.position
		else:
			river_side = 0
			$Player.position = $Side0Position.position
		$Rivers/Board.position = $BoardPosition.position
		$Rivers/Board.show()
	elif in_filter:
		if not with_bottle and $WaterFilter.state ==  0:
			create_dialog("res://assets/dialogs/11-player-try-to-filter-without-bottle.res")
		elif with_bottle and $WaterBottle.empty:
			create_dialog("res://assets/dialogs/10-the-player-try-to-filter-an-empty-bottle.res")
		if with_bottle and not $WaterBottle.empty and not $WaterBottle.clean:
			$WaterFilter.filter_water_bottle()
			with_bottle = false
			emit_signal("bottle_state", 3)
		if not with_bottle and not $WaterBottle.empty and $WaterBottle.clean:
			$WaterFilter.remove_water_bottle()
			with_bottle = true
			emit_signal("bottle_state", 4)
	elif in_board:
		if not board_clean and with_bottle and $WaterBottle.clean:
			cleaning_board = true
			$WaterBottle.use_water()
	elif in_rocket_top:
		if not rocket_top_clean and with_bottle and $WaterBottle.clean:
			cleaning_rocket = true
			create_dialog("res://assets/dialogs/06-as-the-player-pours-water-into-the-dry-mud.res")
			$WaterBottle.use_water()
		elif not rocket_top_clean:
			if with_bottle and not $WaterBottle.empty and not $WaterBottle.clean:
				create_dialog("res://assets/dialogs/05-as-the-player-pours-unfiltered-water-into-the-dry-mud.res")
			else:
				create_dialog("res://assets/dialogs/02-as-the-player-interacts-with-the-dry-mud.res")
	if with_bot and with_mid and with_top and (in_rocket_top or in_rocket_bot or in_rocket_mid):
		get_tree().change_scene("res://CreditScreen/CreditScreen.tscn")
		queue_free()

func _on_Player_drop_bottle():
	if died:
		return
	if with_bottle:
		with_bottle = false
		emit_signal("bottle_state", 3)
		$WaterBottle.position = $Player.position
		$WaterBottle.show()
		$WaterBottle.get_node("CollisionShape2D").disabled = false


func _on_Player_died():
	if not died:
		$DyingTimer.start()
		died = true


func _on_DyingTimer_timeout():
	$DyingTimer.stop()
	get_tree().change_scene("res://GameOver/GameOver.tscn")
	queue_free()


func _on_Player_recharge():
	if not with_bottle:
		return
	if not died and not $WaterBottle.empty and $WaterBottle.clean:
		recharging = true
		$WaterBottle.use_water()
		create_dialog("res://assets/dialogs/08-as-the-player-pours-filtered-water-on-the-plant.res")
	elif not died and not $WaterBottle.empty and not $WaterBottle.clean:
		create_dialog("res://assets/dialogs/07-as-the-player-tries-to-pour-unfiltered-water-on-the-plant.res")

func _on_WaterBottle_used_water():
	if recharging:
		$Player/OxygenTank.value = 100
		recharging = false
	elif cleaning_rocket:
		$RocketUpDirt.queue_free()
		cleaning_rocket = false
		rocket_top_clean = true
	elif cleaning_board:
		$Rivers/BoardDirt.queue_free()
		cleaning_board = false
		board_clean = true

func _on_Player_low_oxygen(state):
	match state:
		OXYGEN_BELOW_THRESH:
			$fundo_f1.stop()
			$low_oxygen.play()
		OXYGEN_ABOVE_THRESH:
			$fundo_f1.play()
			$low_oxygen.stop()

func on_init_dialog(d):
	get_tree().paused = true

func on_end_dialog(d):
	get_tree().paused = false
	d.queue_free()

func create_dialog(file):
	var dialog = load("res://Dialog/Dialog.tscn")
	var d = dialog.instance()
	d.dialog_file = file
	d.connect('dialog_init', self, 'on_init_dialog', [d])
	d.connect('dialog_finished', self, 'on_end_dialog', [d])
	add_child(d)


func on_explosion_explosion_finished():
	create_dialog("res://assets/dialogs/00-explosion.res")
