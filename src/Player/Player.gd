extends KinematicBody2D

export (float) var player_velocity_per_seconds = 250
export (float) var minimal_joy_movement = 0.05

signal died
signal hit(obj)
signal main_button
signal drop_bottle

signal recharge
signal low_oxygen

var can_move
var dir_left

const BOTTLE_EMPTY = 0
const BOTTLE_CLEAN = 1
const BOTTLE_FULL = 2
const BOTTLE_HIDE = 3
const BOTTLE_SHOW = 4

var oxygen_tank
var bottle_icon

var bottle_empty_img = preload("res://WaterBottle/GAgua_Vazia.svg")
var bottle_full_img = preload("res://WaterBottle/GLama_7.svg")
var bottle_clean_img = preload("res://WaterBottle/GAgua_7.svg")

var is_walking
var collision_obj

func _ready():
	bottle_icon = $GUI/HBoxContainer/Counter/Background/Icon
	oxygen_tank = $OxygenTank
	position = get_viewport().size/2
	can_move = true
	is_walking = false
	dir_left = true
	$Sprite.animation = "steady"
	
func on_ran_out_oxygen():
	can_move = false
	emit_signal('died')

func get_input_moviment_from_joy():
	var v = Vector2(
		Input.get_joy_axis(0, JOY_ANALOG_LX),
		Input.get_joy_axis(0, JOY_ANALOG_LY)
	)

	if v.x < minimal_joy_movement and v.x > -minimal_joy_movement:
		v.x = 0
	if v.y < minimal_joy_movement and v.y > - minimal_joy_movement:
		v.y = 0

	return v

func _process(delta):
	$GUI/HBoxContainer/OxygenBar/Count/Background/Number.text = str(round(oxygen_tank.value))
	$GUI/HBoxContainer/OxygenBar/Gauge.value = oxygen_tank.value
    
func _physics_process(delta):
	if not can_move:
		return
		
	var velocity = Vector2()
	if Input.is_action_pressed('player_up'):
		velocity.y = -1
	if Input.is_action_pressed('player_down'):
		velocity.y = 1
	if Input.is_action_pressed('player_left'):
		velocity.x = -1
	if Input.is_action_pressed('player_right'):
		velocity.x = 1
	if Input.is_action_just_pressed('ui_main_button'):
		emit_signal("main_button")
	if Input.is_action_just_pressed('ui_drop_bottle'):
		emit_signal("drop_bottle")
		$Drop.play()
	if Input.is_action_pressed('recharge_ox'):
		emit_signal("recharge")
		
	velocity += get_input_moviment_from_joy()
	
	if not dir_left and velocity.x < 0:
		dir_left = true
		$Sprite.flip_h = false
	elif dir_left and velocity.x > 0:
		dir_left = false
		$Sprite.flip_h = true
		
	var player_velocity = player_velocity_per_seconds
	var current_velocity = velocity.normalized()*player_velocity
	var collision_info = move_and_collide(current_velocity * delta)
	
	if collision_info:
		$Sprite.animation = "steady"
		$WalkFx.stop()		
		is_walking = false
		emit_signal('hit', collision_info.collider)
	else:
		var vel_length = velocity.length_squared()
		if not is_walking and vel_length > 0:
			is_walking = true
			$Sprite.animation = "walking"
			$WalkFx.play()
		elif is_walking and vel_length == 0:
			$Sprite.animation = "steady"
			is_walking = false
			$WalkFx.stop()
		
func _on_WaterBottle_bottle_state(state):
	match state:
		BOTTLE_EMPTY:
			bottle_icon.texture = bottle_empty_img
		BOTTLE_CLEAN:
			bottle_icon.texture = bottle_clean_img
		BOTTLE_FULL:
			bottle_icon.texture = bottle_full_img

			

func _on_Main_bottle_state(state):
	match state:
		BOTTLE_HIDE:
			bottle_icon.hide()
		BOTTLE_SHOW:
			bottle_icon.show()


func _on_WaterBottle_can_move(state):
	can_move = state

func _on_OxygenTank_low_oxygen(state):
	emit_signal("low_oxygen", state)
