#!/usr/bin/zsh
godot=/home/rafael/Downloads/exec/Godot_v3.0.6-stable_x11.64

$godot --export "Linux/X11" missing-home.x86_64 --path ../src
$godot --export "Windows Desktop" missing-home.exe --path ../src
$godot --export "Mac OSX" missing-home.zip --path ../src
mv ../src/missing-home* ../release/
